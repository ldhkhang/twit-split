function splitMessage(text)
{
try{
text = text.trim();
if(text !== null || text !== ""){
    //console.log("LENGTH: "+text.length)
    let splitMessageArr= [];
    let arrayText = text.match(/\S+/g) || null
    if(text.length > 50){
            let tempStr = '';
        for(let i = 0 ; i < arrayText.length;i++){
                let checkStr = tempStr + " " + arrayText[i]
            if(checkStr.length>47){
                splitMessageArr.push(tempStr.trim());
                tempStr = arrayText[i];
            }
            else{   
                tempStr = checkStr; 
            }
            if(i === (arrayText.length-1) && tempStr.length <= 50){
                splitMessageArr.push(tempStr.trim());
            }
        }
         for(let i = 0 ; i < splitMessageArr.length;i++){
            splitMessageArr[i] = (i+1) + "/" + splitMessageArr.length + " " + splitMessageArr[i];
        }
        return splitMessageArr;
    }
    else{
        return [arrayText.join(" ")];
    }
}
else 
return [];
}
catch(error){
    return []
}
}
module.exports = splitMessage