function checkOverFiftyCharInMessage(text){
    text = text.split(" ");
    for(let i = 0; i< text.length ; i++){
        if(text[i].length>50){
            return false;
        }  
    }
    return true; 
}
module.exports = checkOverFiftyCharInMessage;