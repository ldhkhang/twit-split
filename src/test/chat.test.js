const expect = require('expect');
const splitMessage = require("../function/splitMessage");
const checkOverFiftyCharInMessage = require("../function/checkOverFiftyCharInMessage");


describe('TEST SPLIT FUNCTION',
()=>{
    it('SEND CHAT (with empty string)', () => {
        expect(splitMessage("")).toEqual([])
    })
    it('SEND CHAT (with white-space)', () => {
        expect(splitMessage("         ")).toEqual([])
    })
    it('SEND CHAT (with 1 word): "Hello"', () => {
        expect(splitMessage("Hello")).toEqual(["Hello"])
    }),
    it('SEND CHAT (with no longer than 50 char ): "paris in the rain"', () => {
        expect(splitMessage("paris in the rain")).toEqual(["paris in the rain"])
    })
    it('SEND CHAT (with longer than 50 char ): "We could go anywhere, we could do. Anything, girl, whatever the mood we\'re in"', () => {
        expect(splitMessage("We could go anywhere, we could do. Anything, girl, whatever the mood we're in")).toEqual(["1/2 We could go anywhere, we could do. Anything,", "2/2 girl, whatever the mood we're in"])
    })
    it('SEND CHAT (with emoji): "I\'ve been counting my stars 🌟. Cause I will spend my whole life loving you 🌈🌈🌈"', () => {
        expect(splitMessage("I\'ve been counting my stars 🌟. Cause I will spend my whole life loving you 🌈🌈🌈")).toEqual(["1/2 I\'ve been counting my stars 🌟. Cause I will", "2/2 spend my whole life loving you 🌈🌈🌈"])
    })
})

describe('TEST CHECK OVER 50 CHAR', ()=>{
    it('Input with normal word: "Hello"', ()=>{
        expect(checkOverFiftyCharInMessage("Hello")).toBe(true)
    }),
    it('Input nomal string: "Please don\'t see just a boy caught up in dreams and fantasies"', ()=>{
        expect(checkOverFiftyCharInMessage("Please don't see just a boy caught up in dreams and fantasies")).toBe(true)
    }),
    it('Input string with a word longer than 50 char: "Helloooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo, can you hear me?"', ()=>{
        expect(checkOverFiftyCharInMessage("Helloooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo, can you hear me?")).toBe(false)
    })
    it('Input string with a word no longer than 50 char: "Helloooooooooooooooooooooo, can you hear me?"', ()=>{
        expect(checkOverFiftyCharInMessage("Helloooooooooooooooooooooo, can you hear me?")).toBe(true)
    })
    it('Input string with emoji: "Hello ✌️, can you hear me?"', ()=>{
        expect(checkOverFiftyCharInMessage("Hello ✌️, can you hear me?")).toBe(true)
    })
})