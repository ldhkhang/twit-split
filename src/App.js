import React, { Component } from 'react';
import './App.css';
import Chat from './components/chat';
import {toaster} from "evergreen-ui";
class App extends Component {
  componentDidMount() {
    toaster.success("Hello There!!!",
    {
      description: 'Wellcome back. 🤖',
      duration: 2
    })
  }
  render() {
    return (
      <div className="App">
        <Chat />
      </div>
    );
  }
}

export default App;
