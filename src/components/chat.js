import React, {Component} from 'react';
import './../css/chat.css';
import {TextInput, Button, toaster, Pane, majorScale, Heading } from 'evergreen-ui';
import splitMessage from '../function/splitMessage';
import checkOverFiftyCharInMessage from '../function/checkOverFiftyCharInMessage';
export default class Chat extends Component{
constructor(){
    super()
    this.ContentsSeletor = React.createRef();
    this.state = {
        valueInput : '',
        messages: [
            'Try it 😍',
            'Mary Poppins returns 👻'
        ]
    }
    //i down know this 🙀 just kid 
    this.sendChat = this.sendChat.bind(this);
    this.handleTextInPutChange= this.handleTextInPutChange.bind(this);
}

//update if value change
handleTextInPutChange(e) {
    this.setState({valueInput: e.target.value});
  }
// auto scroll to bottom when state has re-render
scrollToBottom() {
    const scrollHeight = this.ContentsSeletor.scrollHeight;
    const height = this.ContentsSeletor.clientHeight;
    const maxScrollTop = scrollHeight - height;
    this.ContentsSeletor.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
  }
// when user click on button SEND this function will check valid and set state, if not ok => show error
sendChat(){
    if(this.state.valueInput.trim().length===0 || this.state.valueInput.length > 280){
        toaster.danger("Lỗi!", {
            id: 'forbidden-action',
            description: 'Tin nhắn rỗng hoặc quá số lượng ký tự cho phép!'
          })
    }
    else if(!checkOverFiftyCharInMessage(this.state.valueInput)){
        toaster.danger("Lỗi!", {
            id: 'forbidden-action',
            description: 'Tin nhắn lỗi định dạng!'
          })
    }
    else{
    let MessageAfterSplit = splitMessage(this.state.valueInput);
    this.setState({valueInput: '',messages: this.state.messages.concat(MessageAfterSplit)})
    }
    this.scrollToBottom();
}
 
componentDidMount(){
    this.scrollToBottom();
}
componentDidUpdate() {
    this.scrollToBottom();
  }
render(){
    const {messages} = this.state;
    return(
        <Pane>
        <Pane
        is="header"
        elevation={1}
        paddingLeft={majorScale(1)}
        paddingRight={majorScale(1)}
        height={64}
        display="flex"
        alignItems="center"
      >
        <Pane
          width={960}
          display="flex"
          alignItems="center"
          marginLeft="auto"
          marginRight="auto"
        >
          <Heading size={500} letterSpacing="1px" fontWeight={700}>
          Tweeter
          </Heading>
        </Pane>
      </Pane>
      <Pane
        is="main"
        marginTop={majorScale(3)}
        width={960}
        display="flex"
        alignItems="center"
        justifyContent="center"
        marginLeft="auto"
        marginRight="auto"
      >
      <Pane is="chatBox" className="chatBox">
        <Pane innerRef={el => {this.ContentsSeletor = el;}} className="contents">
        {
            messages && messages.map((messages, index)=>  <div className="textChat" key={index}>{messages}</div>)
        }
        </Pane>
        <Pane className="chatInput">
        <Pane>
            <TextInput name="text-input-name"
            maxLength="280"
            width="100%" 
            height={48}
            value={this.state.valueInput}
            onChange={this.handleTextInPutChange.bind(this)}
            placeholder=" Enter message..." />
            
            <Button 
            height={majorScale(5)} 
            onClick={this.sendChat} 
            appearance="primary"
            marginRight={16}
            intent="success" 
            marginTop={8}>SEND
            </Button>
        </Pane>
        </Pane>
    </Pane>
      </Pane>
        
</Pane>
    )
}
}